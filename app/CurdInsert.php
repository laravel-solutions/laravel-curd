<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurdInsert extends Model
{
 	protected $fillable = [
 		'text','email', 'password', 'single_select', 'multiple_select', 'textarea', 'checkbox', 'file'
 	];
}
