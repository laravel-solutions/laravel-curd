<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CurdInsert;
use File;

class FormController extends Controller
{
    
    public function index(){
    	return view('index');
    }

    public function store(Request $request){
    	$table = new CurdInsert();

    	$table->text =  $request->input('text');
    	$table->email =  $request->input('email');
    	$table->password =  $request->input('password');
    	$table->single_select =  $request->input('single_select');
    	$table->multiple_select =  $request->input('multiple_select');
    	$table->textarea =  $request->input('textarea');
    	$table->checkbox =  $request->input('checkbox');
    	 if($request->hasFile('file')) {
          $imagePath = $request->file('file');
          $imageName = $imagePath->getClientOriginalName();
          $x = $imagePath->move(public_path('\uploads'),$imageName);
          $table->file = $imageName;
        }

    	$table->save();

    	return redirect()->back()->with('success', 'success');
    }

    public function view(){
      $data = CurdInsert::get();
      return view('view', compact('data'));
    }

    public function edit(Request $request, $id){
      $data = CurdInsert::where('id', $id)->first();
      return view('edit', compact('data'));
    }

     public function delete(Request $request, $id){
      $data = CurdInsert::where('id', $id)->first();
      if ($data != null) {
        $image_path = asset('public/uploads/'.$data->file);
          if($image_path !=NULL) {
            File::delete($image_path);
          }
        $data->delete();
        return redirect()->back()->with('success', 'success');
    }
    return redirect()->back()->with('error', 'error');
    }


        public function update(Request $request, $id){
      $table = new CurdInsert();

      $table->text =  $request->input('text');
      $table->email =  $request->input('email');
      $table->password =  $request->input('password');
      $table->single_select =  $request->input('single_select');
      $table->multiple_select =  $request->input('multiple_select');
      $table->textarea =  $request->input('textarea');
      $table->checkbox =  $request->input('checkbox');
       if($request->hasFile('file')) {
          $imagePath = $request->file('file');
          $imageName = $imagePath->getClientOriginalName();
          $x = $imagePath->move(public_path('\uploads'),$imageName);
          $table->file = $imageName;
        }

        CurdInsert::where('id', $id)
                    ->update(
                              ['text'=>$table->text, 
                              'email'=>$table->email, 
                              'password'=>$table->password,
                              'single_select'=>$table->single_select,
                              'multiple_select'=>$table->multiple_select,
                              'textarea'=>$table->textarea,
                              'checkbox'=>$table->checkbox,
                              'file'=>$table->file
                              ]
                            );

      return redirect()->back()->with('success', 'success');
    }

}
