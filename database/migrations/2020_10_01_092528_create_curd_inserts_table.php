<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurdInsertsTable extends Migration
{
    public function up()
    {
        Schema::create('curd_inserts', function (Blueprint $table) {
            $table->id();
            $table->string('text')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('single_select')->nullable();
            $table->string('multiple_select')->nullable();
            $table->string('textarea')->nullable();
            $table->string('checkbox')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('curd_inserts');
    }
}
