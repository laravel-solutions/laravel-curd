<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('index', 'FormController@index');
Route::post('index', 'FormController@store')->name('store');
Route::get('view', 'FormController@view');
Route::post('delete/{id}', 'FormController@delete');
Route::get('edit/{id}', 'FormController@edit');
Route::post('edit/{id}', 'FormController@update');