<link rel="stylesheet" href="{{ asset('css/app.css')}}"/>

<nav class="navbar fixed-top navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">
      LARAVEL CURD Example
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
     <ul class="navbar-nav mr-auto">

     </ul>

     <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="/index"><button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Add">Add</button></a>
      </li>
    </ul>
  </div>
</div>
</nav>

<br/>
<br/>
<br/>
<div class="container">
  @if(session()->has('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div>
  @endif
  @if(session()->has('error'))
  <div class="alert alert-danger">
    {{ session()->get('error') }}
  </div>
  @endif

  <div class="col-md-12" style="padding-top: 30px;">
    <div class="row">
      <div class="col-sm-1">
      </div>

      <div class="col-md-11">
        @csrf
        <table class="table">
          <thead>
            <tr>
              <th scope="col">SL NO</th>
              <th scope="col">File</th>
              <th scope="col">Input Text</th>
              <th scope="col">Input Email</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $dt)
            <tr>
              <th scope="row">1</th>
              <td><img src="{{ asset('uploads/'.$dt->file) }}" style="width: 40%; height: 40%;"></td>
              <td>{{$dt->text}}</td>
              <td>{{$dt->email}}</td>
              <td>
                <ul class="list-inline m-0">
                  <li class="list-inline-item">
                    <a href="/index"><button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Add">Add</button></a>
                  </li>
                  <form method="get" action ="{{ url('edit', ['id' => $dt->id]) }}" enctype="multipart/form-data">
                  <li class="list-inline-item">
                    <button class="btn btn-success btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</button>
                  </li>
                </form>
                  <form method="post" action ="{{ url('/delete', ['id' => $dt->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <li class="list-inline-item">
                     <button class="btn btn-danger btn-sm rounded-0" type="submit" data-toggle="tooltip" data-placement="top" title="Delete">Delete</button>
                   </li>
                 </form>
               </ul>
             </td>
             @endforeach
           </tr>
         </tbody>
       </table>
     </div>
   </div>
 </div>
</div>


