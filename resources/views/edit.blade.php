
<link rel="stylesheet" href="{{ asset('css/app.css')}}"/>


    <nav class="navbar fixed-top navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
          LARAVEL CURD Example
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="/view">View</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container">
  <br/>
  <br/>
  <br/>
        @if(session()->has('success'))
          <div class="alert alert-success">
              {{ session()->get('success') }}
          </div>
        @endif
  <div class="col-md-12" style="padding-top: 30px;">
    <div class="row">



      <div class="col-md-1">
      </div>
      <div class="col-md-10">
        <form method="post" action ="{{ url('/edit', ['id' => $data->id]) }}" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlInput1">Example Text</label>
                <input type="text" class="form-control" name="text" placeholder="Enter Text" value="{{$data->text}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Example Email </label>
                <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{$data->email}}">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlSelect1">Example select</label>
                <select class="form-control" name="single_select">
                  <option value="1"  {{ old('single_select', $data->single_select) == 1 ? 'selected' : '' }}>1</option>
                  <option value="2"  {{ old('single_select', $data->single_select) == 2 ? 'selected' : '' }}>2</option>
                  <option value="3"  {{ old('single_select', $data->single_select) == 3 ? 'selected' : '' }}>3</option>
                  <option value="4"  {{ old('single_select', $data->single_select) == 4 ? 'selected' : '' }}>4</option>
                  <option value="5"  {{ old('single_select', $data->single_select) == 5 ? 'selected' : '' }}>5</option>

                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" value="{{$data->password}}">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Example textarea</label>
                <textarea class="form-control" name="textarea" rows="3" >{{$data->textarea}}</textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlSelect2">Example multiple select</label>
                <select multiple class="form-control" name="multiple_select">
                  <option value="1"  {{ old('multiple_select', $data->multiple_select) == 1 ? 'selected' : '' }}>1</option>
                  <option value="2"  {{ old('multiple_select', $data->multiple_select) == 2 ? 'selected' : '' }}>2</option>
                  <option value="3"  {{ old('multiple_select', $data->multiple_select) == 3 ? 'selected' : '' }}>3</option>
                  <option value="4"  {{ old('multiple_select', $data->multiple_select) == 4 ? 'selected' : '' }}>4</option>
                  <option value="5"  {{ old('multiple_select', $data->multiple_select) == 5 ? 'selected' : '' }}>5</option>
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlFile1">Example file input</label>
                <input type="file" class="form-control-file" name="file">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="checkbox">
                <label class="form-check-label" for="exampleCheck1" value="">Check me out</label>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group" style="text-align: center">
               <button type="submit" class="btn btn-primary">Submit</button>
             </div>
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>
